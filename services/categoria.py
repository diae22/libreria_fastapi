from models.libro import Libro as LibroModel
from models.categoria import Categoria as CategoriaModel
from schemas.categoria import Categoria

class CategoriaService:
    def __init__(self, db) -> None:
        self.db = db

    def get_categorias(self):
        return self.db.query(CategoriaModel).all()
    
    def get_categoria_by_id(self, id: int):
        return self.db.query(CategoriaModel).filter(CategoriaModel.id == id).first()
    
    def create_categoria(self, categoria: Categoria):
        existing_categoria = self.db.query(CategoriaModel).filter(CategoriaModel.nombre == categoria.nombre).first()
        if existing_categoria:
            return False
        new_categoria = CategoriaModel(nombre=categoria.nombre)
        self.db.add(new_categoria)
        self.db.commit()
        return True

    def delete_categoria(self, id: int):
        categoria = self.db.query(CategoriaModel).filter(CategoriaModel.id == id).first()
        if not categoria:
            return {}
        libros = self.db.query(LibroModel).filter(LibroModel.categoriaId == id).all()
        if libros:
            return {"libros":len(libros)}
        self.db.delete(categoria)
        self.db.commit()
        return {"nombre":categoria.nombre}

    def update_categoria(self, id: int, categoria: Categoria):
        categoria_to_update = self.db.query(CategoriaModel).filter(CategoriaModel.id == id).first()
        categoria_name = self.db.query(CategoriaModel).filter(CategoriaModel.nombre == categoria.nombre).first()
        if not categoria_to_update:
            return {}
        if categoria_name:
            return {"yaExiste": True}
        categoria_to_update.nombre = categoria.nombre
        self.db.commit()
        return {"correcto": True}