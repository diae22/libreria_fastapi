from models.libro import Libro as LibroModel
from models.categoria import Categoria as CategoriaModel
from schemas.libro import Libro

class LibroService():
    def __init__(self, db) -> None:
        self.db = db

    def get_libros(self):
        result = self.db.query(LibroModel, CategoriaModel.nombre).join(CategoriaModel).all()
        return result
    
    def get_libro(self, id:int):
        result = self.db.query(LibroModel, CategoriaModel.nombre).join(LibroModel.categoria)\
                .filter(LibroModel.id == id).first()
        return result
    
    def get_libros_by_categoria(self, categoria:str):
        result = self.db.query(LibroModel, CategoriaModel.nombre)\
            .join(CategoriaModel)\
            .filter(CategoriaModel.nombre == categoria)\
            .all()
        return result
    
    def create_libro(self, libro: Libro):
        categoria = self.db.query(CategoriaModel).filter(CategoriaModel.nombre == libro.categoria).first()
        if not categoria:
            return False
        libro2 = libro.model_dump()
        del libro2["categoria"]
        libro2["categoriaId"] = categoria.id
        new_libro = LibroModel(**libro2)
        self.db.add(new_libro)
        self.db.commit()
        return True

    def delete_libro(self, id: int):
        result = self.db.query(LibroModel).filter(LibroModel.id==id).first()
        self.db.delete(result)
        self.db.commit()
        return result.titulo
    
    def update_libro(self, id, libro: Libro):
        result = self.db.query(LibroModel).filter(LibroModel.id==id).first()
        if not result:
            return {"libro": False}
        categoria = self.db.query(CategoriaModel).filter(CategoriaModel.nombre == libro.categoria).first()
        if not categoria:
            return {"categoria": False}
        result.titulo = libro.titulo
        result.autor = libro.autor
        result.año = libro.año
        result.categoriaId = categoria.id
        result.numPaginas = libro.numPaginas
        self.db.commit()
        return {"actualizacion": True}