from fastapi.responses import JSONResponse
from typing import List
from fastapi import APIRouter, Depends, Path, Query
from middlewares.jwt_bearer import JWTBearer
from config.database import Session
from models.libro import Libro as LibroModel
from models.categoria import Categoria as CategoriaModel
from schemas.libro import Libro
from services.libro import LibroService
from fastapi.encoders import jsonable_encoder

libro_router = APIRouter()

def agregarNombreCategoria(result):
    libros = []
    for libro, nombreCategoria in result:
        libros.append({
            "id": libro.id,
            "titulo": libro.titulo,
            "autor": libro.autor,
            "año": libro.año,
            "categoria": nombreCategoria,
            "numPaginas": libro.numPaginas
        })
    return libros

# Mostrar todos los libros
@libro_router.get('/libros', tags=['libros'], response_model = List[Libro], status_code = 200)
def get_libros() -> List[Libro]:
    db = Session()
    result = LibroService(db).get_libros()
    db.close()
    if not result:
        return JSONResponse(status_code = 404, content={'message': "No se encontraron libros"})
    libros = agregarNombreCategoria(result)
    return JSONResponse(status_code=200, content=jsonable_encoder(libros))

# Buscar libro por id
@libro_router.get('/libros/{id}', tags=['libros'], response_model = List[Libro], status_code = 200)
def get_libro(id: int = Path(ge = 1)) -> List[Libro]:
    db = Session()
    result = LibroService(db).get_libro(id)
    db.close()
    if not result:
        return JSONResponse(status_code = 404, content={'message': "Libro no encontrado"})
    libros = agregarNombreCategoria([result])
    return JSONResponse(status_code=200, content=jsonable_encoder(libros))

# Buscar libros por categoria (Revisado por el God del Mewing)
@libro_router.get('/libros/', tags=['libros'], response_model = List[Libro], status_code = 200)
def get_libros_by_categoria(categoria: str = Query(min_length = 1, max_length = 30)) -> List[Libro]:
    db = Session()
    result = LibroService(db).get_libros_by_categoria(categoria)
    db.close()
    if not result:
        return JSONResponse(status_code = 404, content={'message': "No se encontraron libros"})
    libros = agregarNombreCategoria(result)
    return JSONResponse(status_code=200, content=jsonable_encoder(libros))

# Agregar un libro
# (TAMBIEN HECHO POR EL DIOS DEL MEWING)  revivan la grasa :v (livliv)   
@libro_router.post('/libros/', tags=['libros'], response_model= dict, status_code=200)
def create_libro(libro: Libro)->dict:
    db = Session()
    result = db.query(CategoriaModel).filter(CategoriaModel.nombre == libro.categoria).first()
    if not result:
        return JSONResponse(status_code = 404, content={"message": "La categoría no existe."})
    LibroService(db).create_libro(libro)
    db.close()
    return JSONResponse(status_code= 200, content={"message": libro.titulo +" se registro con exito!"})

# Eliminar un libro
# HECHO POR HAZIEL DEV (EL DIOS DEL MEWING)
# Editado por livers (skibidi mewing sigma digital circus chamba fortnite)
@libro_router.delete('/libros/{id}', tags=['libros'], response_model = dict, status_code= 200)
def delete_libro(id: int = Path(ge = 1)) -> dict:
    db = Session()
    result = LibroService(db).get_libro(id)
    if not result:
        return JSONResponse(status_code= 404, content= {"message": "no se encontro el id: "+ str(id)})
    titulo = LibroService(db).delete_libro(id)
    db.close()
    return JSONResponse(status_code= 200, content= {"message": titulo + " se borró con exito"})
            
# Actualizar un libro
# livliv (¿eeeeeeees confuso verdad? sin embargo, skibidi mewing sigma está mal, todo el globo de texto te lo hace saber. te notas chad, con pensamientos en decadencia. un sentimiento de que el prime no volverá a ser lo mismo.)
@libro_router.put('/libros/{id}', tags=['libros'], response_model = dict, status_code = 200)
def update_libro(id: int, libro:Libro) -> dict:
    db = Session()
    result = LibroService(db).update_libro(id, libro)
    db.close()
    if "libro" in result:
        return JSONResponse(status_code = 404, content = {"message": "No se encontró el libro."})
    if "categoria" in result:
        return JSONResponse(status_code = 404, content = {"message": "No se encontró la categoria"})
    return JSONResponse(status_code=200, content = {"message": "Se actualizó correctamente el libro."})