from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from typing import List
from fastapi import APIRouter, Depends, Path
from middlewares.jwt_bearer import JWTBearer
from models.categoria import Categoria as CategoriaModel
from models.libro import Libro as LibroModel
from config.database import Session
from schemas.categoria import Categoria
from services.categoria import CategoriaService

categoria_router = APIRouter()

# Mostrar todas las categorias
@categoria_router.get('/categorias', tags=['categorias'], response_model = List[Categoria], status_code = 200)
def get_categorias() -> List[Categoria]:
    db = Session()
    result = CategoriaService(db).get_categorias()
    db.close()
    if not result:
        return JSONResponse(status_code = 404, content={'message': "No se encontraron categorias"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

# Buscar categoria por id
@categoria_router.get('/categorias/{id}', tags=['categorias'], response_model = List[Categoria], status_code = 200)
def get_categoria_by_id(id: int = Path(ge = 1)) -> List[Categoria]:
    db = Session()
    result = CategoriaService(db).get_categoria_by_id(id)
    db.close()
    if not result:
        return JSONResponse(status_code = 404, content={'message': "Categoria con el id:"+str(id)+" No encontrada :c"})
    return JSONResponse(status_code=200, content=jsonable_encoder([result]))

@categoria_router.post('/categorias/', tags=['categorias'], response_model = dict, status_code = 200)
def create_categoria(categoria:Categoria) -> dict:
    db = Session()
    result = CategoriaService(db).create_categoria(categoria)
    db.close()
    if not result:
        return JSONResponse(status_code = 409, content={'message': "Ya existe esta categoria"})
    return JSONResponse(status_code= 200, content={"message": "La categoría: "+ categoria.nombre +" se registro con exito."})

# Borrar categoria (Hecho por el dios del MEWING HAZIELDEV))
@categoria_router.delete('/categorias/{id}', tags=['categorias'], response_model= dict, status_code=200)
def delete_cat(id:int = Path(ge = 1)) -> dict:
    db = Session()
    result = CategoriaService(db).delete_categoria(id)
    db.close()
    if not result:
        return JSONResponse(status_code= 404, content = {"message":"No se encontro la categoria con el id: ("+str(id)+") :c"})
    if "libros" in result:
        librosCount = result["libros"]
        return JSONResponse(status_code=409, content= {"message":"No se puede borrar porque hay ("+str(librosCount)+") libros en esta categoria"})
    nombreCategoria = result["nombre"]
    return JSONResponse(status_code= 200, content={"message":"Se borro la categoria ("+nombreCategoria+")"})
    
# Actualizar categoria
@categoria_router.put('/categorias/{id}', tags=['categorias'], response_model = dict, status_code = 200)
def update_categoria(id: int, categoria: Categoria) -> dict:
    db = Session()
    result = CategoriaService(db).update_categoria(id, categoria)
    db.close()
    if not result:
        return JSONResponse(status_code = 404, content = {"message": "No se encontró la categoría."})
    if "yaExiste" in result:
        return JSONResponse(status_code = 409, content = {"message": "Ya existe una categoria con este nombre"})
    return JSONResponse(status_code=200, content = {"message": "Se actualizó correctamente la categoría."})
