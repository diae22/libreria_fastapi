from pydantic import BaseModel, Field
from typing import Optional

class Categoria(BaseModel):
    id: Optional[int] = None
    nombre: str = Field(min_length=1, max_length=30)

    class Config:
        json_schema_extra = {
            "example": {
                "nombre": "Sci-fi",
            }
        }