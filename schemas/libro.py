from pydantic import BaseModel, Field
from typing import Optional

class Libro(BaseModel):
    id: Optional[int] = None
    titulo: str = Field(min_length = 1, max_length = 40)
    autor: str  = Field(min_length = 1, max_length = 30)
    año: int = Field(le=2024)
    categoria: str = Field(min_length=1, max_length=30)
    numPaginas: int = Field(ge = 1, le = 3000)

    class Config:
        json_schema_extra = {
            "example": {
                "titulo": "Mi libro",
                "autor":"Pepe",
                "año": 2024,
                "categoria": "Sci-fi",
                "numPaginas": 99
            }
        }